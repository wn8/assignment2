from scipy import misc
import numpy as np
import tensorflow as tf
# import random
# import matplotlib.pyplot as plt
# import matplotlib as mp
import time

# --------------------------------------------------
# setup

def weight_variable(shape):
    '''
    Initialize weights
    :param shape: shape of weights, e.g. [w, h ,Cin, Cout] where
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters
    Cout: the number of filters
    :return: a tensor variable for weights with initial values
    '''

    # IMPLEMENT YOUR WEIGHT_VARIABLE HERE

    # random initialization
    initial = tf.truncated_normal(shape, stddev=0.1)
    W = tf.Variable(initial)

    return W

def bias_variable(shape):
    '''
    Initialize biases
    :param shape: shape of biases, e.g. [Cout] where
    Cout: the number of filters
    :return: a tensor variable for biases with initial values
    '''

    # IMPLEMENT YOUR BIAS_VARIABLE HERE
    initial = tf.constant(0.1, shape=shape)
    b = tf.Variable(initial)

    return b

def conv2d(x, W):
    '''
    Perform 2-D convolution
    :param x: input tensor of size [N, W, H, Cin] where
    N: the number of images
    W: width of images
    H: height of images
    Cin: the number of channels of images
    :param W: weight tensor [w, h, Cin, Cout]
    w: width of the filters
    h: height of the filters
    Cin: the number of the channels of the filters = the number of channels of images
    Cout: the number of filters
    :return: a tensor of features extracted by the filters, a.k.a. the results after convolution
    '''

    # IMPLEMENT YOUR CONV2D HERE
    h_conv = tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

    return h_conv

def max_pool_2x2(x):
    '''
    Perform non-overlapping 2-D maxpooling on 2x2 regions in the input data
    :param x: input data
    :return: the results of maxpooling (max-marginalized + downsampling)
    '''

    # IMPLEMENT YOUR MAX_POOL_2X2 HERE
    h_max = tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

    return h_max


def variable_summaries(var, name):
    """Attach a lot of summaries to a Tensor."""
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.scalar_summary('mean/' + name, mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.scalar_summary('stddev/' + name, stddev)
        tf.scalar_summary('max/' + name, tf.reduce_max(var))
        tf.scalar_summary('min/' + name, tf.reduce_min(var))
        tf.histogram_summary(name, var)

def optimizer_type(lr, mo, opt_type):

    return{
            'GDO': tf.train.GradientDescentOptimizer(lr),
            'Adam': tf.train.AdamOptimizer(lr),
            'Momentum': tf.train.MomentumOptimizer(lr, mo),
            'Adagrad': tf.train.AdagradOptimizer(lr),
    }[opt_type]

# Specify training parameters
train_dir = './trainCifar_results/train' # directory where the results from the training are saved
test_dir = './trainCifar_results/test' # directory where the results from the testing are saved

start_time = time.time() # start timing

ntrain = 1000 # number of training samples per class
ntest = 100 # number of testing samples per class
nclass = 10 # number of classes
imsize = 28 # image size 28*28
nchannels = 1 # number of channels in each image
batchsize = 100 # training batch size

max_step = 3000 # the maximum iterations. After max_step iterations, the training will stop.
learning_rate = 1e-4 # learning rate
momentum = 0.9 # This parameter is only used in Momentum-based optimizer
opt_type = 'Adam' # optimizer type

# global_step = tf.Variable(0, trainable=False)
# starter_learning_rate = 0.001
# learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step, 100, 0.96, staircase=True)

Train = np.zeros((ntrain*nclass, imsize, imsize, nchannels))
Test = np.zeros((ntest*nclass, imsize, imsize, nchannels))
LTrain = np.zeros((ntrain*nclass, nclass))
LTest = np.zeros((ntest*nclass, nclass))

itrain = -1
itest = -1
for iclass in range(0, nclass):
    for isample in range(0, ntrain):
        path = './CIFAR10/Train/%d/Image%05d.png' % (iclass, isample)
        im = misc.imread(path) # 28 by 28
        im = im.astype(float)/255
        itrain += 1
        Train[itrain,:,:,0] = im
        LTrain[itrain,iclass] = 1 # 1-hot lable
    for isample in range(0, ntest):
        path = './CIFAR10/Test/%d/Image%05d.png' % (iclass, isample)
        im = misc.imread(path) # 28 by 28
        im = im.astype(float)/255
        itest += 1
        Test[itest,:,:,0] = im
        LTest[itest,iclass] = 1 # 1-hot lable

sess = tf.InteractiveSession()

tf_data = tf.placeholder(tf.float32, shape=[None, 28, 28, 1]) #  tf variable for the data, remember
                                                            #  shape is [None, width, height, numberOfChannels]
tf_labels = tf.placeholder(tf.float32, shape=[None, 10])    # tf variable for labels

# --------------------------------------------------
# model
#create your model

# first convolutional layer
W_conv1 = weight_variable([5, 5, 1, 32])
b_conv1 = bias_variable([32])
net_input1 = conv2d(tf_data, W_conv1) + b_conv1
h_conv1 = tf.nn.relu(net_input1)
h_pool1 = max_pool_2x2(h_conv1)
# Visualize the statistics of the activations on the test images
# tf.histogram_summary('Layer1/activations', h_conv1)
variable_summaries(h_conv1, 'Layer1/activations')

# Visualize the first convolutional layer's weights
img_summary = tf.image_summary('filter1', tf.transpose(W_conv1, [3, 0, 1, 2]), max_images=32)

# second convolutional layer
W_conv2 = weight_variable([5, 5, 32, 64])
b_conv2 = bias_variable([64])
net_input2 = conv2d(h_pool1, W_conv2) + b_conv2
h_conv2 = tf.nn.relu(net_input2)
h_pool2 = max_pool_2x2(h_conv2)
# Visualize the statistics of the activations on the test images
# tf.histogram_summary('Layer2/activations', h_conv2)
variable_summaries(h_conv2, 'Layer2/activations')

# densely connected layer
W_fc1 = weight_variable([7 * 7 * 64, 1024])
b_fc1 = bias_variable([1024])
h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

# dropout
keep_prob = tf.placeholder(tf.float32)
h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

# softmax
W_fc2 = weight_variable([1024, 10])
b_fc2 = bias_variable([10])
y_conv = tf.nn.softmax(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)

# --------------------------------------------------
# loss
# set up the loss, optimization, evaluation, and accuracy
with tf.name_scope('cross_entropy'):
    cross_entropy = tf.reduce_mean(-tf.reduce_sum(tf_labels * tf.log(y_conv), reduction_indices=[1]))
    tf.scalar_summary('cross_entropy', cross_entropy)

optimizer = optimizer_type(learning_rate, momentum, opt_type).minimize(cross_entropy)


with tf.name_scope('accuracy'):
    correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(tf_labels, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    tf.scalar_summary('accuracy', accuracy)


# Build the summary operation based on the TF collection of Summaries.
summary_op = tf.merge_all_summaries()

# Instantiate a SummaryWriter to output summaries and the Graph.
train_writer = tf.train.SummaryWriter(train_dir, sess.graph)
test_writer = tf.train.SummaryWriter(test_dir, sess.graph)

# --------------------------------------------------
# optimization

sess.run(tf.initialize_all_variables())
batch_xs = np.zeros((batchsize, imsize, imsize, nchannels)) #setup as [batchsize, width, height, numberOfChannels] and use np.zeros()
batch_ys = np.zeros((batchsize, nclass)) #setup as [batchsize, the how many classes]
nsamples = ntrain * nclass
for i in range(max_step): # try a small iteration size once it works then continue
    perm = np.arange(nsamples)
    np.random.shuffle(perm)
    for j in range(batchsize):
        batch_xs[j,:,:,:] = Train[perm[j],:,:,:]
        batch_ys[j,:] = LTrain[perm[j],:]
    if i % 100 == 0:
        # calculate train accuracy and print it
        train_accuracy = accuracy.eval(feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 1.0})
        test_accuracy = accuracy.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0})
        print("step %d, training accuracy %g, test accuracy %g" % (i, train_accuracy, test_accuracy))

        # Update the events file which is used to monitor the training
        summary_str = sess.run(summary_op, feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 0.5})
        train_writer.add_summary(summary_str, i)
        summary_str = sess.run(summary_op, feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 0.5})
        test_writer.add_summary(summary_str, i)
        # summary_writer.flush()

    optimizer.run(feed_dict={tf_data: batch_xs, tf_labels: batch_ys, keep_prob: 0.5}) # dropout only during training

train_writer.close()
test_writer.close()

# --------------------------------------------------
# test

print("test accuracy %g" % accuracy.eval(feed_dict={tf_data: Test, tf_labels: LTest, keep_prob: 1.0}))

sess.close()

stop_time = time.time()
print('The training takes %f second to finish'%(stop_time - start_time))
