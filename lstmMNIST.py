import tensorflow as tf 
from tensorflow.python.ops import rnn, rnn_cell
import numpy as np 

from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets('MNIST_data', one_hot=True) #call mnist function

# Specify training parameters
train_dir = './lstmMNIST_results/train' # directory where the results from the training are saved
test_dir = './lstmMNIST_results/test' # directory where the results from the testing are saved

learningRate = 0.001 # learning rate
trainingIters = 100000 # training iterations
batchSize = 128 # batch size
displayStep = 10 # display step

nInput = 28 # we want the input to take the 28 pixels
nSteps = 28 # every 28 pixel chunks
nHidden = 128 # number of neurons for the RNN
nClasses = 10 # number of classes

x = tf.placeholder('float', [None, nSteps, nInput])
y = tf.placeholder('float', [None, nClasses])

weights = {
    'out': tf.Variable(tf.random_normal([nHidden, nClasses]))
}

biases = {
    'out': tf.Variable(tf.random_normal([nClasses]))
}

def RNN(x, weights, biases):
    x = tf.transpose(x, [1,0,2])
    x = tf.reshape(x, [-1, nInput])
    x = tf.split(0, nSteps, x) # configuring so you can get it as needed for the 28 pixels

    # RNN
    # basic_cell = rnn_cell.BasicRNNCell(nHidden) # find which lstm to use in the documentation
    # outputs, states = rnn.rnn(basic_cell, x, dtype=tf.float32) # for the rnn where to get the output and hidden state

    # LSTM
    lstm_cell = rnn_cell.BasicLSTMCell(nHidden, forget_bias=1.0)
    outputs, states = rnn.rnn(lstm_cell, x, dtype=tf.float32)

    # GRU
    # gru_cell = rnn_cell.GRUCell(nHidden)
    # outputs, states = rnn.rnn(gru_cell, x, dtype=tf.float32)

    return tf.matmul(outputs[-1], weights['out']) + biases['out']


pred = RNN(x, weights, biases)

#optimization
#create the cost, optimization, evaluation, and accuracy
#for the cost softmax_cross_entropy_with_logits seems really good
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))
optimizer = tf.train.AdamOptimizer(learning_rate=learningRate).minimize(cost)

correctPred = tf.equal(tf.argmax(pred,1), tf.argmax(y,1))
accuracy = tf.reduce_mean(tf.cast(correctPred, tf.float32))

# Add a scalar summary for the snapshot loss.
tf.scalar_summary('cross_entropy', cost)
tf.scalar_summary('accuracy', accuracy)

# Build the summary operation based on the TF collection of Summaries.
summary_op = tf.merge_all_summaries()

init = tf.initialize_all_variables()

with tf.Session() as sess:
    sess.run(init)
    step = 1

    # Instantiate a SummaryWriter to output summaries and the Graph.
    train_writer = tf.train.SummaryWriter(train_dir, sess.graph)
    test_writer = tf.train.SummaryWriter(test_dir, sess.graph)

    testData = mnist.test.images.reshape((-1, nSteps, nInput))
    testLabel = mnist.test.labels

    while step* batchSize < trainingIters:
        batchX, batchY = mnist.train.next_batch(batchSize) # mnist has a way to get the next batch
        batchX = batchX.reshape((batchSize, nSteps, nInput))

        sess.run(optimizer, feed_dict={x: batchX, y: batchY})

        if step % displayStep == 0:
            acc = sess.run(accuracy, feed_dict={x: batchX, y: batchY})
            loss = sess.run(cost, feed_dict={x: batchX, y: batchY})
            print("Iter " + str(step*batchSize) + ", Minibatch Loss= " + \
                  "{:.6f}".format(loss) + ", Training Accuracy= " + \
                  "{:.5f}".format(acc))

            # Update the events file which is used to monitor the training
            summary_str = sess.run(summary_op, feed_dict={x: batchX, y: batchY})
            train_writer.add_summary(summary_str, step)
            summary_str = sess.run(summary_op, feed_dict={x: testData, y: testLabel})
            test_writer.add_summary(summary_str, step)

        step += 1
    print('Optimization finished')

    print("Testing Accuracy:", sess.run(accuracy, feed_dict={x: testData, y: testLabel}))
